<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>SHAF</title>
		<link rel="stylesheet" type="text/css" href="style.css">
		<link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@500&display=swap" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	</head>
	<body>
		
		<!-- sidebar -->
		<input type="checkbox" id="check">
		<div class="sidebar">
			<ul>
                <li><a href="index.html">Home</a></li>
				<li><a href="about.php">About</a></li>
				<li><a href="contact.php">Contact</a></li>
			</ul>
		</div>

		<!-- bagian header -->
		<header>
			<div class="container">
				<h1><a href="index.html">ZU.SITE</a></h1>
				<ul>
                    <li><a href="index.html">Home</a></li>
					<li><a href="about.php">About</a></li>
					<li><a href="contact.php">Contact</a></li>
				</ul>

				<!-- menu mobile -->
				<label for="check" class="mobile-menu"><i class="fas fa-bars fa-2x"></i></label>
			</div>
		</header>

		<section id="contact">
			<div class="container">
                <br><br><br><br>
				<h3>Contact</h3>
				<div class="col-3">
					<h4>Alamat</h4>
					<p>Dungsuru, Pilangrejo, Nglipar, Gunungkidul, DIY</p>
				</div>

				<div class="col-3">
					<h4>Email</h4>
					<p>shaflyzuhernata.sz@gmail.com</p>
				</div>

				<div class="col-3">
					<h4>Telp/Hp</h4>
					<p>083840434626</p>
				</div>	
			</div>
	    </section>

        <section id="tamu">
        <div class="container">
            <br><br><br>
            <h3>Buku Tamu</h3>
            <div align="center">
                <p>Silakan isi buku tamu di bawah ini untuk meninggalkan komentar Anda!</p>
            </div>

            <div class="form">
            <form name="form1" method="post" action="proses.php"> 
                <table width="30%" border="0" align="center"> 
                    <tr> 
                        <td>Nama Lengkap</td> 
                        <td><input name="nama" type="text" id="nama"></td> 
                    </tr> 
                
                    <tr> 
                        <td>Alamat</td> 
                        <td><input name="alamat" type="text" id="alamat"></td> 
                    </tr>

                    <tr> 
                        <td>E-Mail</td> 
                        <td><input name="email" type="text" id="email"></td> </tr> 
                    </tr> 
                    
                    <div class="status">
                    <tr> 
                        <td>Status</td> 
                        <td>
                            <select name="status" id="status"> 
                                <option value="Menikah">Menikah</option> 
                                <option value="Single">Single</option> 
                            </select>
                        </td> 
                    </tr>
                    </div> 

                    <tr> 
                    <td>Komentar</td> 
                    <td><textarea name="komentar" id="komentar"></textarea></td> 
                    </textarea>
                    </tr> 

                    <tr> 
                        <td>&nbsp;</td> 
                        <td>
                            <input type="submit" name="Submit" value="Kirim">
                            <input type="reset" name="Submit2" value="Batal"> 
                        </td> 
                    </tr> 
                </table> 

            </form> 
            <div align="center"><a href="lihat.php"><u>Lihat Buku Tamu</u><br><br><br></a>
            <br>
            <form enctype="multipart/form-data" method="post" action="latihan33_upload.php"> 
                Foto Anda : <t><t><input type="file" name="fupload"><br><br> 
                <input type=submit value=Upload> 
            </form> <br><br>
            </div>
        </div>
        </div>
        </div>
        
		<!-- bagian footer -->
		<footer>
			<div class="container">
				<br><small>Copyright &copy; 2021 Shafly Zuhernata</small>
			</div>
		</footer>
		
	</body>
</html>