<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>SHAF</title>
		<link rel="stylesheet" type="text/css" href="style.css">
		<link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@500&display=swap" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	</head>
	<body>
		
		<!-- sidebar -->
		<input type="checkbox" id="check">
		<div class="sidebar">
			<ul>
                <li><a href="index.html">Home</a></li>
				<li><a href="about.php">About</a></li>
				<li><a href="contact.php">Contact</a></li>
			</ul>
		</div>

		<!-- bagian header -->
		<header>
			<div class="container">
				<h1><a href="index.html">ZU.SITE</a></h1>
				<ul>
                    <li><a href="index.html">Home</a></li>
					<li><a href="about.php">About</a></li>
					<li><a href="contact.php">Contact</a></li>
				</ul>

				<!-- menu mobile -->
				<label for="check" class="mobile-menu"><i class="fas fa-bars fa-2x"></i></label>
			</div>
		</header>

		<!-- bagian about -->
		<section id="about">
            <div class="about">
                <div class="container">
                    <h3>About</h3>
                    <p>Saya Shafly Zuhernata. Saya berasal dari Kabupaten Gunungkidul, Daerah Istimewa Yogyakarta. Saya sekarang kuliah di Prodi <b>Teknik Informatika</b> Fakultas <b>Teknologi Industri Universitas Ahmad Dahlan</b>. Saya menyukai mengedit gambar dan video. Saya masih mempelajari dua hal tersebut dan tentunya saya juga tertarik dengan web desain dan semoga di suatu dapat menjadi Front-End Developer dan UI/UX Designer.</p>
                </div>
            </div>
		</section>

        <section id="about">
			<div class="container">
				<h3>Visitors Counter</h3>
                    <div class="cout">
                        <?php
                            $file_counter = "counter.txt";
                            $f_open = fopen($file_counter, "r+");
                            $hitung = fread($f_open, filesize($file_counter));
                            echo "<center>Anda pengunjung ke-".$hitung;
                            fclose($f_open);

                            $f_open = fopen($file_counter, "w+");
                            $hitung = $hitung + 1;
                            fwrite($f_open, $hitung, strlen($hitung));
                            fclose($f_open);
                        ?>
                    <div>
            </div>
	    </section>

		<!-- bagian footer -->
		<footer>
			<div class="container">
				<br><small>Copyright &copy; 2021 Shafly Zuhernata</small>
			</div>
		</footer>
		
	</body>
</html>