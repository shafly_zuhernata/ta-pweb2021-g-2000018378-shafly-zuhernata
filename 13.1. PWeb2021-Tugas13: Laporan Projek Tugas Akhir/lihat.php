<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>SHAF</title>
		<link rel="stylesheet" type="text/css" href="style.css">
		<link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@500&display=swap" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	</head>
	<body>
		
		<!-- sidebar -->
		<input type="checkbox" id="check">
		<div class="sidebar">
			<ul>
                <li><a href="index.html">Home</a></li>
				<li><a href="about.php">About</a></li>
				<li><a href="contact.php">Contact</a></li>
			</ul>
		</div>

		<!-- bagian header -->
		<header>
			<div class="container">
				<h1><a href="index.html">ZU.SITE</a></h1>
				<ul>
                    <li><a href="index.html">Home</a></li>
					<li><a href="about.php">About</a></li>
					<li><a href="contact.php">Contact</a></li>
				</ul>

				<!-- menu mobile -->
				<label for="check" class="mobile-menu"><i class="fas fa-bars fa-2x"></i></label>
			</div>
		</header>
        
        <section id="about">
            <div class="about">
                <div class="container">
                    <h3>Daftar Buku Tamu</h3>
                    <?php 
                        echo "<head><title>My Guest Book</title></head>"; 
                        $fp = fopen("guestbook.txt","r"); 
                        echo "<table border=0>"; 

                        while ($isi = fgets($fp,80)) 
                        { 
                        $pisah = explode("|",$isi); 
                        echo "<tr><td>Nama </td><td>: $pisah[0]</td></tr>"; 
                        echo "<tr><td>Alamat </td><td>: $pisah[1]</td></tr>"; 
                        echo "<tr><td>Email </td><td>: $pisah[2]</td></tr>"; 
                        echo "<tr><td>Status </td><td>: $pisah[3]</td></tr>"; 
                        echo "<tr><td>Komentar </td><td>: $pisah[4]</td></tr> 
                        <tr><td>&nbsp;<hr></td><td>&nbsp;<hr></td></tr>"; 
                        } 

                        echo "</table>"; 
                        echo "<br><a href=contact.php> Kembali"; 
                    ?>
                </div>
            </div>
		</section>

		<!-- bagian footer -->
		<footer>
			<div class="container">
				<br><small>Copyright &copy; 2021 Shafly Zuhernata</small>
			</div>
		</footer>
		
	</body>
</html>