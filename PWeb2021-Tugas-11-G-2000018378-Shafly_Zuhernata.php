<!DOCTYPE HTML>  
<html>
<head>
<title>Penanganan Form</title>
<style>
.error {color: #FF0000;}
</style>
</head>
<body>  

<?php
$nameErr = $emailErr = $genderErr = $websiteErr = "";
$name = $email = $gender = $comment = $website = "";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["name"])) {
    $nameErr = "Nama Harus Diisi";
  } else {
    $name = test_input($_POST["name"]);
    if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
      $nameErr = "Hanya Huruf dan Spasi"; 
    }
  }
  
  if (empty($_POST["email"])) {
    $emailErr = "Email diperlukan";
  } else {
    $email = test_input($_POST["email"]);
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $emailErr = "Format Email Tidak Valid"; 
    }
    
    if (empty($_POST["comment"])) {
        $comment = "";
      } else {
        $comment = test_input($_POST["comment"]);
      }

  if (empty($_POST["gender"])) {
    $genderErr = "Jenis Kelamin Harus Diisi";
  } else {
    $gender = test_input($_POST["gender"]);
  }
} 
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>

<p><span class="error">* wajib diisi.</span></p>
<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">  
  Nama: <input type="text" name="name" value="<?php echo $name;?>">
  <span class="error">* <?php echo $nameErr;?></span>
  <br><br>
  E-mail: <input type="text" name="email" value="<?php echo $email;?>">
  <span class="error">* <?php echo $emailErr;?></span>
  <br><br>
  Komentar: <br><textarea name="comment" rows="5" cols="40"><?php echo $comment;?></textarea>
  <br><br>
  Jenis Kelamin:   
  <input type="radio" name="gender" <?php if (isset($gender) && $gender=="Perempuan") echo "checked";?> value="Perempuan">P
  <input type="radio" name="gender" <?php if (isset($gender) && $gender=="Laki-Laki") echo "checked";?> value="Laki-Laki">L
  <span class="error">* <?php echo $genderErr;?></span>
  <br><br>
  <input type="submit" name="submit" value="Submit">  
</form>

<?php
echo "<br>";
echo "Nama : ".$name;
echo "<br>";
echo "Email : ".$email;
echo "<br>";
echo $comment;
echo "<br>";
echo "Jenis Kelamin : ".$gender;
?>

</body>
</html>