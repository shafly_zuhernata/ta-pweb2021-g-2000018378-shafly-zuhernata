<!DOCTYPE html>
<html>
<head>
    <title>Tugas 09</title>
    <link rel="stylesheet" type="text/css" href="style09.css">
</head>
<body>

    <div class="content">
        <header>
        <h1 class="judul">Sintak PHP Dasar</h1>
        </header>
    </div>

    <div class="badan">
    <form action="hasil.php" method="post">
        <div class="text-sub">
            <p><b>Listing Program 9.1</b></p>
        </div>

        <?php
            $gaji = 1000000;
            $pajak = 0.1;
            $thp = $gaji - ($gaji*$pajak);
            
            echo "Gaji sebelum pajak = Rp. $gaji <br>";
            echo "Gaji yang dibawa pulang = Rp. $thp";
        ?>

        <br><br>

        <div class="text-sub">
            <p><b>Listing Program 9.2</b></p>
        </div>

        <?php
            $a = 5;
            $b = 4;

            echo "$a == $b : ". ($a == $b);
            echo "<br>$a != $b : ". ($a != $b);
            echo "<br>$a > $b : ". ($a > $b);
            echo "<br>$a < $b : ". ($a < $b);
            echo "<br>($a == $b) && ($a > $b) : ". (($a != $b) && ($a > $b));
            echo "<br>($a == $b) && ($a > $b) : ". (($a != $b) && ($a > $b));
        ?>

        <br>
    </div>

    <footer class="foot">
		<center><p class = "copy">Copyright &copy; 2021 Shafly Zuhernata | 2000018378</p><center>
	</footer>

</body>
</html>